import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {

  constructor() { }


  config = {
    value: false,
    name: "",
    disabled: false,
    height: 25,
    width: 100,
    margin: 3,
    fontSize: 10,
    speed: 300,
    color: {
      checked: "#FFC000",
      unchecked: "#232323"
    },
    switchColor: {
      checked: "#232323",
      unchecked: "#FFC000"
    },
    labels: {
      unchecked: "Business",
      checked: "Personal"
    },
    checkedLabel: "Personal",
    uncheckedLabel: "Business",
    fontColor: {
      checked: "#232323",
      unchecked: "#ffffff"
    }
  };

  ngOnInit(): void {
  }

}
