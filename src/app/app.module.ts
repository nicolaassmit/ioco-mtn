import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';

import { NavigationComponent } from './navigation/navigation.component';
import { NgToggleModule } from 'ng-toggle-button';
import { CarouselComponent } from './carousel/carousel.component';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    CarouselComponent
  ],
  imports: [
    BrowserModule,
    NgToggleModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
